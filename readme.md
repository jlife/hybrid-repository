**hybrid-repository** - to show how to work with mongo by hybrid methods

## Requirements ##

###Please install to local maven repo the next artifacts:###
 
1. [common-entity](https://bitbucket.org/jlife/common-entity) 
2. [common-repository](https://bitbucket.org/jlife/common-repository)

Instructions about install are available in readme.md in these projects

## Conventions ##

* Custom interface always we will call with the prefix **Custom**(UserRepositoryCustom)
* Custom realization of this interface we will call with the prefix **Impl**(UserRepositoryImpl)


## Building ##

    ./gradlew clean build
    
  