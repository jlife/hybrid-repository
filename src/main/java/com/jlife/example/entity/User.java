package com.jlife.example.entity;

import com.jlife.common.entity.Entity;

import java.util.List;

/**
 * @author Dzmitry Misiuk
 */
public class User extends Entity {

    private String email;
    private List<String> roles;
    private String description;

    public User() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
