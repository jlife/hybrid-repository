package com.jlife.example.repository;

import com.jlife.example.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 * @author Dzmitry Misiuk
 */
public class UserRepositoryImpl implements UserRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public UserRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<User> findUserByRole(String role) {
        Query atLeastRoleQuery = Query.query(Criteria.where("roles").is(role));
        return mongoTemplate.find(atLeastRoleQuery, User.class);
    }
}
