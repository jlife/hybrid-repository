package com.jlife.example.repository;

import com.jlife.example.entity.User;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * @author Dzmitry Misiuk
 */
public interface UserRepositoryCustom {
    List<User> findUserByRole(String role);
}
