package com.jlife.example.repository;

import com.jlife.common.repository.EntityRepository;
import com.jlife.example.entity.User;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

/**
 * @author Dzmitry Misiuk
 */
public interface UserRepository  extends EntityRepository<User>, UserRepositoryCustom {

    User findByEmail(String email);

    List<User> findByDescriptionRegex(String descriptionRegex);

    @Query("{'description' : {'$regex' : ?0}}")
    List<User> findByDescriptionRegexOwnQuery(String descriptionRegex);

}
