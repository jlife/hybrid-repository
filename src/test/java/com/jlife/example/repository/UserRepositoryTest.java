package com.jlife.example.repository;

import com.jlife.example.entity.User;
import com.jlife.example.repository.UserRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author Dzmitry Misiuk
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/repository-test.xml"})
public class UserRepositoryTest {

    public static final String TEST_EMAIL = "test";
    public static final String DESCRIPTION_A = "aaaaaa1";
    public static final String DESCRIPTION_B = "bbbbb1";
    public static final String ROLE_1 = "ROLE1";
    public static final String ROLE_2 = "ROLE2";
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MongoTemplate mongoTemplate;
    private String userId1;
    private String userId2;

    @Before
    public void createUsers() {
        this.cleanDb();
        User user = new User();
        user.setEmail(TEST_EMAIL);
        user.setDescription(DESCRIPTION_A);
        user.setRoles(Arrays.asList(ROLE_1, ROLE_2));
        userRepository.save(user);
        userId1 = user.getId();

        user = new User();
        user.setRoles(Arrays.asList(ROLE_1));
        user.setDescription(DESCRIPTION_B);
        userRepository.save(user);
        userId2 = user.getId();
    }

    @Test
    public void testUpdate(){
        User user = userRepository.findOne(userId1);
        user.setDescription("changed description");
        userRepository.save(user);
    }

    @Test
    public void testFindByEmail() {
        User readUser = userRepository.findByEmail(TEST_EMAIL);
        assertNotNull(readUser);
        assertEquals(userId1, readUser.getId());
    }

    @Test
    public void testFindByRole1() {
        List<User> usersWithRole1 = userRepository.findUserByRole(ROLE_1);
        assertNotNull(usersWithRole1);
        assertEquals(2, usersWithRole1.size());
    }

    @Test
    public void testFindByRole2() {
        List<User> usersWithRole2 = userRepository.findUserByRole(ROLE_2);
        assertNotNull(usersWithRole2);
        assertEquals(1, usersWithRole2.size());

    }

    @Test
    public void testFindByDescriptionRegex() {
        List<User> users = userRepository.findByDescriptionRegex("a");
        assertNotNull(users);
        assertEquals(1, users.size());
        assertEquals(userId1, users.get(0).getId());
    }

    @Test
    public void testFindByDescriptionRegexOwnQuery() {
        List<User> users = userRepository.findByDescriptionRegex("a");
        assertNotNull(users);
        assertEquals(1, users.size());
        assertEquals(userId1, users.get(0).getId());

        users = userRepository.findByDescriptionRegex("1");
        assertNotNull(users);
        assertEquals(2, users.size());
    }

    @After
    public void cleanDb() {
        mongoTemplate.dropCollection(User.class);
    }
}